// 1. Create fruits collection

db.fruits.insertMany([
	{
		"name": "Banana",
		"supplier": "Farmer Fruits Inc.",
		"stocks": 30,
		"price": 20,
		"onSale": true
	},
	{
		"name": "Mango",
		"supplier": "Mango Magic Inc.",
		"stocks": 50,
		"price": 70,
		"onSale": true
	},
	{
		"name": "Dragon Fruit",
		"supplier": "Farmer Fruits Inc.",
		"stocks": 10,
		"price": 60,
		"onSale": true
	},
	{
		"name": "Grapes",
		"supplier": "Fruity Co.",
		"stocks": 30,
		"price": 100,
		"onSale": true
	},
	{
		"name": "Apple",
		"supplier": "Apple Valley",
		"stocks": 0,
		"price": 20,
		"onSale": false
	},
	{
		"name": "Papaya",
		"supplier": "Fruity Co.",
		"stocks": 15,
		"price": 60,
		"onSale": true
	}
]);

// 2. Count total number of fruits on sale
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$count: "onSaleFruits"}
]);

// 3. Count total number of fruits with stock more than 20

db.fruits.aggregate([
	{$match: {"stocks": { $gt: 20 } }},
	{$count: "stockAbove20"}
]);

// 4. Average price of fruits onSale per supplier
db.fruits.aggregate([
	{ $group: {_id: "$supplier", avgPrice: { $avg: "$price"}} },
]);

// 5. Highest price fo a fruit per supplier

db.fruits.aggregate([
	{ $group: {_id: "$supplier", maxPrice: { $max: "$price"}} },
]);

// 6. Lowest price of a fruit per supplier

db.fruits.aggregate([
	{ $group: {_id: "$supplier", minPrice: { $min: "$price"}} },
]);